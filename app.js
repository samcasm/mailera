var express = require("express");
var app = express();
var isEmail = require("isemail");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var Mailgun = require('mailgun-js');
var passport = require('passport');
var LocalStrategy = require("passport-local");
var passportLocalMongoose = require("passport-local-mongoose");
var User = require("./models/user.js");

/////////  MONGODB CONNECTION  /////////////
mongoose.connect("mongodb://localhost/mailera-project");
//Setting a schema for the database
var personSchema = new mongoose.Schema({
    name:String,
    email:String,
    interest:String,
    list_number:Number
});
var Person = mongoose.model("Person",personSchema);

//////       MAILGUN API DECLARATIONS      //////////
var api_key = 'key-336406f8a0416d00670d8e5447015208';
var domain = 'sandboxbcea6e89ec6f46b994ad83b4c0f00352.mailgun.org';
var from_who = 'theadmin@me.com';  //Admin mail id

app.use(require("express-session")({
    secret:"Can be anything literally",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname+"/public"));
app.set("view engine","ejs");


passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
passport.use(new LocalStrategy(User.authenticate()));

/////         ROUTES          /////
//CLIENT HOME
app.get("/",function(req,res){
    res.render("home");
});

//CLIENT DATA COLLECTION ROUTE
//VALIDATES EMAIL AND INPUTS IN MONGODB
app.post("/user-data",function(req,res){
    var list_number;
    if(req.body.interest=="Fashion"){
        list_number = 1;
    }else if(req.body.interest=="Leisure"){
        list_number = 2;
    }else if(req.body.interest=="Sports"){
        list_number = 3;
    }else if(req.body.interest=="Travel"){
        list_number = 4;
    }
    
    if(isEmail.validate(req.body.email)){
        Person.create({
            name:req.body.name,
            email:req.body.email,
            interest:req.body.interest,
            list_number:list_number
        },function(err,person){
            if(err){
                console.log(err);
            }else{
                console.log(person,"successfully logged");
                res.render("thankyou.ejs",{interest:req.body.interest});
            }
        });
        
    }else{
        res.send("Not a valid email buddy");
    }
});

//ADMIN HOME
//FILTERS USER DATA AND MAPS ONLY REQUIRED DATA FOR DISPLAY

app.get("/admin-login",function(req,res){
    res.render("admin-login");
});


app.post("/admin-login",passport.authenticate("local",{
    successRedirect:"/admin",
    failureRedirect: "/admin-login"
}),function(req, res) {
    //Used to register the admin one time
    // User.register(new User({username:req.body.username}),req.body.password,function(err,user){
    //     if(err){
    //         console.log(err);
            
    //     }
    //     else{
    //         passport.authenticate("local")(req, res, function(){
    //             res.redirect("/admin");
    //         });
    //     }
    // });
    
});

app.get("/admin",isLoggedIn,function(req,res){
        Person.find({},function(err,thelist){
            if(err){
                console.log("Error");
            }else{
               
               var output = thelist.map(function(item){
                return {
                    name: item.name,
                    email: item.email,
                    interest: item.interest,
                    list_number: item.list_number
                    
                    };
                });
                
               res.render("admin",{output:output});
            }
        });
 
});

//FIRES WHEN A LIST IS SELECTED 
//RENDERS MAIL BODY 
app.post("/newmail",isLoggedIn,function(req,res){
    var emailIds= new Array();
    console.log(req.body); 
    //Select all emails in list and fill the TO input in the MAIL BODY
    if(req.body.choice=="Fashion"){
       Person.find({list_number:1},function(err,thelist){
           if(err){
               console.log(err);
           }else{
               console.log(thelist);
               thelist.forEach(function(person){
                   emailIds.push(person.email);
               });
               res.render("newmail",{choice:req.body.choice,emailIds:emailIds});
           }
       });
   }else if(req.body.choice=="Leisure"){
       Person.find({list_number:2},function(err,thelist){
           if(err){
               console.log(err);
           }else{
               console.log(thelist);
               thelist.forEach(function(person){
                   emailIds.push(person.email);
               });
               res.render("newmail",{choice:req.body.choice,emailIds:emailIds});
           }
       });
   }else if(req.body.choice=="Sports"){
       Person.find({list_number:3},function(err,thelist){
           if(err){
               console.log(err);
           }else{
               console.log(thelist);
               thelist.forEach(function(person){
                   emailIds.push(person.email);
               });
               res.render("newmail",{choice:req.body.choice,emailIds:emailIds});
           }
       });
   }else if(req.body.choice=="Travel"){
       Person.find({list_number:4},function(err,thelist){
           if(err){
               console.log(err);
           }else{
               console.log(thelist);
               thelist.forEach(function(person){
                   emailIds.push(person.email);
               });
               res.render("newmail",{choice:req.body.choice,emailIds:emailIds});
           }
       });
   }
    
});

//SEND MAILS USING MAILGIN API METHOD
app.post("/submit",isLoggedIn,function(req,res){
   console.log(req.body); 
   var mailgun = new Mailgun({apiKey: api_key, domain: domain});

    var data = {
      from: from_who,
      to: req.body.to,
      subject: req.body.subject,
      text: req.body.text
    };

    mailgun.messages().send(data, function (err, body) {
        if (err) {
            res.render('error', { error : err});
            console.log("got an error: ", err);
        }
        else {
            res.render("done");
            }
    });
});

function isLoggedIn(req,res,next){
    if(req.isAuthenticated()){
        console.log("Authenticated");
        return next();
    }
        res.redirect("/admin-login");
}

///////       LISTEN        //////////////
app.listen(process.env.PORT,process.env.IP,function(){
   console.log("Mailera is up and listening"); 
});