#README#

### What is this repository for? ###

* Quick summary:
This repo is for an application called MAILERA that is a micro crm that uses Mailgun Api to fire mails to various customer lists.


### How do I get set up? ###

* Summary of set up:
Install **cloud9 IDE** and clone this repo.
Setup **mongodb** in the root directory of cloud9 itself: https://community.c9.io/t/setting-up-mongodb/1717

* Dependencies:
**All dependencies are stated in the package.json file in the repo**

* Deployment instructions:
**Register yourself** as an authorized user for Mailgun Api testing. Prevents spammers.

Go to this link: https://mailgun.com/app/testing/recipients

**Enter login details**: username: samir7kutty@gmail.com,
                     password: password123

Other already authorized mail ids for testing purposes are: 
sam7casm@gmail.com, sam77casm@gmail.com , samir7kutty@gmail.com

Click on Invite new recipent and add your mail ID and verify.
This is done to prevent spam. You can now view the mail that is fired to you, when it is.

On cloud9 terminal run the app.js file: $ node app.js
"Mailera is up and running"

Preview it!

## The entire project is mainly built on NODE.JS ##

## Page Flow: ## 
**User interface:**
         The first page "/" is where the user enters his data, the data gets stored in the database after the email is validated and a thankyou.ejs is rendered. Thats all for the User interface.

**Admin interface:**
         The admin can then login in by going to the "/admin-login" route or any other admin route as they all will redirect to the login page if admin is not authenticated. 
**username : admin**,
**password : password**


Once authenticated the admin can select from the lists of customers to send mail to. Mail form appears with prefilled to(customer emails) data. Once submitted the done.ejs is rendered. And you will receive the mail sent to your mail id! Cheers :D

**Bugs: Avoid refreshing the page** after submitting forms as emails have been validated but already existing duplicate mail ids are not filtered for bulk send testing purposes.
